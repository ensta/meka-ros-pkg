/* Auto-generated by genmsg_cpp for file /home/meka/mekabot/meka-ros-pkg/meka_trajectory/msg/TrajActionGoal.msg */
#ifndef MEKA_TRAJECTORY_MESSAGE_TRAJACTIONGOAL_H
#define MEKA_TRAJECTORY_MESSAGE_TRAJACTIONGOAL_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "std_msgs/Header.h"
#include "actionlib_msgs/GoalID.h"
#include "meka_trajectory/TrajGoal.h"

namespace meka_trajectory
{
template <class ContainerAllocator>
struct TrajActionGoal_ {
  typedef TrajActionGoal_<ContainerAllocator> Type;

  TrajActionGoal_()
  : header()
  , goal_id()
  , goal()
  {
  }

  TrajActionGoal_(const ContainerAllocator& _alloc)
  : header(_alloc)
  , goal_id(_alloc)
  , goal(_alloc)
  {
  }

  typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
   ::std_msgs::Header_<ContainerAllocator>  header;

  typedef  ::actionlib_msgs::GoalID_<ContainerAllocator>  _goal_id_type;
   ::actionlib_msgs::GoalID_<ContainerAllocator>  goal_id;

  typedef  ::meka_trajectory::TrajGoal_<ContainerAllocator>  _goal_type;
   ::meka_trajectory::TrajGoal_<ContainerAllocator>  goal;


  typedef boost::shared_ptr< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::meka_trajectory::TrajActionGoal_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct TrajActionGoal
typedef  ::meka_trajectory::TrajActionGoal_<std::allocator<void> > TrajActionGoal;

typedef boost::shared_ptr< ::meka_trajectory::TrajActionGoal> TrajActionGoalPtr;
typedef boost::shared_ptr< ::meka_trajectory::TrajActionGoal const> TrajActionGoalConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::meka_trajectory::TrajActionGoal_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace meka_trajectory

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::meka_trajectory::TrajActionGoal_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > {
  static const char* value() 
  {
    return "aee77e81e3afb8d91af4939d603609d8";
  }

  static const char* value(const  ::meka_trajectory::TrajActionGoal_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xaee77e81e3afb8d9ULL;
  static const uint64_t static_value2 = 0x1af4939d603609d8ULL;
};

template<class ContainerAllocator>
struct DataType< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > {
  static const char* value() 
  {
    return "meka_trajectory/TrajActionGoal";
  }

  static const char* value(const  ::meka_trajectory::TrajActionGoal_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > {
  static const char* value() 
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
\n\
Header header\n\
actionlib_msgs/GoalID goal_id\n\
TrajGoal goal\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: actionlib_msgs/GoalID\n\
# The stamp should store the time at which this goal was requested.\n\
# It is used by an action server when it tries to preempt all\n\
# goals that were requested before a certain time\n\
time stamp\n\
\n\
# The id provides a way to associate feedback and\n\
# result message with specific goal requests. The id\n\
# specified must be unique.\n\
string id\n\
\n\
\n\
================================================================================\n\
MSG: meka_trajectory/TrajGoal\n\
# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
trajectory_msgs/JointTrajectory trajectory\n\
\n\
================================================================================\n\
MSG: trajectory_msgs/JointTrajectory\n\
Header header\n\
string[] joint_names\n\
JointTrajectoryPoint[] points\n\
================================================================================\n\
MSG: trajectory_msgs/JointTrajectoryPoint\n\
float64[] positions\n\
float64[] velocities\n\
float64[] accelerations\n\
duration time_from_start\n\
";
  }

  static const char* value(const  ::meka_trajectory::TrajActionGoal_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct HasHeader< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct HasHeader< const ::meka_trajectory::TrajActionGoal_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.header);
    stream.next(m.goal_id);
    stream.next(m.goal);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct TrajActionGoal_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::meka_trajectory::TrajActionGoal_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::meka_trajectory::TrajActionGoal_<ContainerAllocator> & v) 
  {
    s << indent << "header: ";
s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "goal_id: ";
s << std::endl;
    Printer< ::actionlib_msgs::GoalID_<ContainerAllocator> >::stream(s, indent + "  ", v.goal_id);
    s << indent << "goal: ";
s << std::endl;
    Printer< ::meka_trajectory::TrajGoal_<ContainerAllocator> >::stream(s, indent + "  ", v.goal);
  }
};


} // namespace message_operations
} // namespace ros

#endif // MEKA_TRAJECTORY_MESSAGE_TRAJACTIONGOAL_H


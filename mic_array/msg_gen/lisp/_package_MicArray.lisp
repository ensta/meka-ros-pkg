(cl:in-package mic_array-msg)
(cl:export '(MIC_ENERGY-VAL
          MIC_ENERGY
          ANGLE-VAL
          ANGLE
          MAG-VAL
          MAG
))
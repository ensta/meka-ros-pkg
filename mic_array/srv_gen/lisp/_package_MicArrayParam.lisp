(cl:in-package mic_array-srv)
(cl:export '(GAINS-VAL
          GAINS
          BIAS-VAL
          BIAS
          WINDOW_TIME-VAL
          WINDOW_TIME
          THRESHOLD-VAL
          THRESHOLD
          SLEW_RATE-VAL
          SLEW_RATE
          RESP-VAL
          RESP
))